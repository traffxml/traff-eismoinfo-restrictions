/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-restrictions library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.restrictions;

import org.json.JSONObject;

public class LtRestrictionsRestriction {
	public final String restrictionType;
	public final String value;
	public final String transportType;

	public static LtRestrictionsRestriction fromJson(JSONObject object) {
		String restrictionType = object.optString("restrictionType");
		String value = object.optString("value");
		String transportType = object.optString("transportType");
		return new LtRestrictionsRestriction(restrictionType, value, transportType);
	}

	public LtRestrictionsRestriction(String restrictionType, String value, String transportType) {
		super();
		this.restrictionType = restrictionType;
		this.value = value;
		this.transportType = transportType;
	}
}
