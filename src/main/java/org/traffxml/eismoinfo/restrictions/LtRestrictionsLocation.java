/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-restrictions library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.restrictions;

import java.util.ArrayList;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.LatLon;

public class LtRestrictionsLocation {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	public final LatLon[] polyline;
	public final String direction;
	public final String street;
	public final String locationDescription;

	static LtRestrictionsLocation fromJson(JSONObject object) {
		ArrayList<LatLon> polyline = null;
		String direction = null;
		String street = null;
		String locationDescription = null;

		String polylineString = object.optString("polyline");
		if (polylineString != null) {
			polyline = new ArrayList<LatLon>();
			String [] polylineCoords = polylineString.split(" ");
			for (int i = 0; i < polylineCoords.length; i += 2)
				try {
					float lon = Float.valueOf(polylineCoords[i]);
					float lat = Float.valueOf(polylineCoords[i + 1]);
					polyline.add(new LatLon(lat, lon));
				} catch (NumberFormatException e) {
					LOG.warn("Dropping {} {}: not a valid coordinate pair",
							polylineCoords[i], polylineCoords[i + 1]);
				}
		}
		direction = object.optString("direction");
		street = object.optString("street");
		locationDescription = object.optString("locationDescription");

		return new LtRestrictionsLocation(polyline.toArray(new LatLon[0]), direction, street,
				locationDescription);
	}

	public LtRestrictionsLocation(LatLon[] polyline, String direction, String street, String locationDescription) {
		super();
		this.polyline = polyline;
		this.direction = direction;
		this.street = street;
		this.locationDescription = locationDescription;
	}
}
