/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-restrictions library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.restrictions.input;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.eismoinfo.restrictions.LtRestrictionsFeed;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.input.DataSource;

public class LtRestrictionsDataSource extends DataSource {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	/* Do not use the MethodHandles hack here, in order to support Android */
	static final Logger LOG = LoggerFactory.getLogger(LtRestrictionsDataSource.class);

	public LtRestrictionsDataSource(String id, String url, Properties properties) {
		super(id, url, properties);
		// TODO Auto-generated constructor stub
	}

	private Date lastUpdate = null;

	@Override
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * Returns the update interval for the source in seconds.
	 * 
	 * <p>This is the interval at which the source provides updated data. Polling more frequently than that will
	 * only yield redundant data and should be avoided.
	 * 
	 * <p>A value of zero indicates that the source may update events at any time.
	 * 
	 * <p>This source always returns 0.
	 */
	@Override
	public int getMinUpdateInterval() {
		return 0;
	}

	@Override
	public boolean needsExistingMessages() {
		return true;
	}

	@Override
	public Collection<TraffMessage> poll(Collection<TraffMessage> oldMessages, int pollInterval) {
		URL url = getUrl();
		LtRestrictionsFeed feed = null;
		try {
			InputStream stream = url.openStream();
			feed = LtRestrictionsFeed.parseJson(stream);
			lastUpdate = new Date();
			return feed.toTraff(id, oldMessages);
		} catch (MalformedURLException e) {
			LOG.debug("{}", e);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
		return null;
	}

}
