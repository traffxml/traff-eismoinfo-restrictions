/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-restrictions library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.restrictions;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffMessage;

public class LtRestrictionsFeed {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	public final LtRestrictionsMessage[] messages;

	public static LtRestrictionsFeed parseJson(InputStream stream) {
		List<LtRestrictionsMessage> messages = new ArrayList<LtRestrictionsMessage>();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int i;

		try {
			i = stream.read();
			while (i != -1) {
				if (i >= 32) outputStream.write(i);
				i = stream.read();
			}
			stream.close();
		} catch (IOException e) {
			LOG.debug("{}", e);
			System.exit(1);
		}

		JSONTokener tokener = new JSONTokener(outputStream.toString());
		while (true) {
			Object object = null;
			try {
				object = tokener.nextValue();
			} catch (JSONException e) {
				LOG.debug("{}", e);
			}
			if (object == null)
				break;
			if (object instanceof JSONArray) {
				for (i = 0; i < ((JSONArray) object).length(); i++) {
					Object child;
					try {
						child = ((JSONArray) object).get(i);
					} catch (JSONException e) {
						LOG.warn("Failed to retrieve object #{} from top-level array", i);
						LOG.debug("{}", e);
						continue;
					}
					if (child instanceof JSONObject)
						messages.add(LtRestrictionsMessage.fromJson((JSONObject) child));
					else
						LOG.warn("Got instance of {} in top-level array, expected JSONObject",
								child.getClass().getName());
				}
			} else
				LOG.warn("Got instance of {} at top level, expected JSONArray",
						object.getClass().getName());
		}
		return new LtRestrictionsFeed(messages.toArray(new LtRestrictionsMessage[0]));
	}

	/**
	 * Converts the feed to a list of TraFF messages.
	 * 
	 * @param oldMessages Previously received messages
	 */
	public List<TraffMessage> toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages) {
		Map<String, TraffMessage> outMap = new HashMap<String, TraffMessage>();
		if (messages != null)
			for (LtRestrictionsMessage message : messages) {
				TraffMessage traff = message.toTraff(sourcePrefix, oldMessages);
				if (traff != null)
					outMap.put(traff.id, traff);			}
		/* Add cancellations for all unmatched old messages */
		if (oldMessages != null)
			for (TraffMessage oldMessage : oldMessages)
				if (oldMessage.id.startsWith(sourcePrefix + ":")
						&& !outMap.containsKey(oldMessage.id)) {
					TraffMessage.Builder builder = new TraffMessage.Builder();
					builder.setId(oldMessage.id);
					builder.setReceiveTime(oldMessage.receiveTime);
					builder.setUpdateTime(new Date());
					Date expirationTime = oldMessage.expirationTime;
					if (expirationTime == null)
						expirationTime = oldMessage.endTime;
					builder.setExpirationTime(expirationTime);
					builder.setCancellation(true);
					outMap.put(oldMessage.id, builder.build());
				}
		List<TraffMessage> res = new ArrayList<TraffMessage>();
		res.addAll(outMap.values());
		return res;
	}

	public LtRestrictionsFeed(LtRestrictionsMessage[] messages) {
		super();
		this.messages = messages;
	}
}
