/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-restrictions library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.eismoinfo.restrictions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.lib.milestone.Distance;
import org.traffxml.lib.milestone.Junction;
import org.traffxml.lib.milestone.JunctionList;
import org.traffxml.lib.milestone.JunctionUtils;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.DimensionQuantifier;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.RoadClass;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.traff.WeightQuantifier;

public class LtRestrictionsMessage {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	/** Minimum distance of the via point from either endpoint. */
	private static final double VIA_MIN_DIST = 1.0;

	/** Minimum length to determine a via point. */
	private static final double VIA_MIN_LENGTH = 3.0;

	/**
	 * Map of junctions.
	 */
	static JunctionList junctionsEismoinfo;
	static JunctionList junctionsOsm;

	static {
		/* Read junction data sets */
		try {
			junctionsEismoinfo = new JunctionList(LtRestrictionsMessage.class.getResourceAsStream("junctions-eismoinfo.csv"),
					',', ';', "lat", "lon", "roadRef", null, "name", "km", Distance.UNIT_KM);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}

		try {
			junctionsOsm = new JunctionList(LtRestrictionsMessage.class.getResourceAsStream("junctions-osm.csv"),
					'\t', ';', "@lat", "@lon", "road_ref", null, "name", "milestone", Distance.UNIT_KM);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
	}

	static Map<String, Set<BoundingBox>> motorwayBboxes = new HashMap<String, Set<BoundingBox>>();

	static {
		/* A1: Klaipėda—Kaunas, Kaunas–Vilnius */
		Set<BoundingBox> set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(54.9634422f, 21.2275298f, 55.7328223f, 23.8624136f));
		set.add(new BoundingBox(54.6841780f, 23.9994504f, 54.9309817f, 25.0560650f));
		motorwayBboxes.put("A1", set);

		/* Vilnius-Panevėžys */
		set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(54.7713607f, 24.3627749f, 55.6645785f, 25.1917797f));
		motorwayBboxes.put("A2", set);

		/* Marijampolė–Kaunas */
		set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(54.5973773f, 23.3969578f, 54.7807138f, 23.7791276f));
		motorwayBboxes.put("A5", set);

		/* Radviliškis–Šiauliai */
		set = new HashSet<BoundingBox>();
		set.add(new BoundingBox(55.8442342f, 23.4012382f, 55.9184609f, 23.5009620f));
		motorwayBboxes.put("A9", set);
	}

	static Map<String, TraffLocation.RoadClass> defaultRoadClassMappings = new HashMap<String, TraffLocation.RoadClass>();

	static {
		defaultRoadClassMappings.put("A1", RoadClass.MOTORWAY);
		defaultRoadClassMappings.put("A2", RoadClass.MOTORWAY);
		defaultRoadClassMappings.put("A5", RoadClass.TRUNK);
		defaultRoadClassMappings.put("A9", RoadClass.TRUNK);
	}

	public final String id; // nonstandard label
	public final String type;
	public final Date creationtime;
	public final Date updatetime;
	public final Date starttime;
	public final Date endtime;
	public final String description;
	public final String shortDescription;
	public final Object schedule; // TODO not yet implemented
	public final LtRestrictionsLocation location;
	public final Object source; // TODO not yet implemented
	public final String parentEvent;
	public final String subtype; // nonstandard values
	public final LtRestrictionsRestriction[] restrictions; // nonstandard member

	/**
	 * Parses a timestamp in ISO 8601 format.
	 * 
	 * @param iso8601Date The timestamp
	 * @return A date corresponding to the timestamp supplied
	 */
	private static Date parseDate(String iso8601Date) {
		DateFormat iso8601Format;
		if (iso8601Date != null)
			try {
				try {
					iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.ROOT);
					return iso8601Format.parse(iso8601Date);
				} catch (IllegalArgumentException e) {
					/* workaround for older Java versions which don't support this format */
					iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ROOT);
					if (iso8601Date.endsWith("Z"))
						iso8601Date = iso8601Date.replace("Z", "+0000");
					else
						iso8601Date = iso8601Date.replaceAll("([+-]\\d\\d):(\\d\\d)\\s*$", "$1$2");
					return iso8601Format.parse(iso8601Date);
				}
			} catch (ParseException e) {
				LOG.warn("Not a valid ISO8601 timestamp: " + iso8601Date + ", ignoring");
			}
		return null;
	}

	static LtRestrictionsMessage fromJson(JSONObject object) {
		String id = null; // nonstandard label
		String type = null;
		Date creationtime = null;
		Date updatetime = null;
		Date starttime = null;
		Date endtime = null;
		String description = null;
		String shortDescription = null;
		Object schedule = null;
		LtRestrictionsLocation location = null;
		Object source = null; // TODO not yet implemented
		String parentEvent = null;
		String subtype = null; // nonstandard values
		ArrayList<LtRestrictionsRestriction> restrictions = null; // nonstandard member
		Object child = null;

		id = object.optString("_id", null);
		type = object.optString("type", null);
		creationtime = parseDate(object.optString("creationtime"));
		updatetime = parseDate(object.optString("updatetime"));
		starttime = parseDate(object.optString("starttime"));
		endtime = parseDate(object.optString("endtime"));
		description = object.optString("description", null);
		shortDescription = object.optString("short_description", null);
		// TODO schedule
		try {
			child = object.getJSONObject("location");
			if (child instanceof JSONObject)
				location = LtRestrictionsLocation.fromJson((JSONObject) child);
			else
				LOG.warn("Got instance of {} for location, expected JSONObject",
						child.getClass().getName());
		} catch (JSONException e) {
			// NOP
		}
		// TODO source
		parentEvent = object.optString("parent_event", null);
		subtype = object.optString("subtype", null);
		try {
			JSONArray restrictionsArray = object.getJSONArray("restrictions");
			restrictions = new ArrayList<LtRestrictionsRestriction>();
			for (int i = 0; i < restrictionsArray.length(); i++) {
				child = restrictionsArray.get(i);
				if (child instanceof JSONObject)
					restrictions.add(LtRestrictionsRestriction.fromJson((JSONObject) child));
				else
					LOG.warn("Got instance of {} for restrictions, expected JSONArray",
							child.getClass().getName());
			}
		} catch (JSONException e) {
			// NOP
		}

		return new LtRestrictionsMessage(id, type, creationtime, updatetime, starttime, endtime, description,
				shortDescription, schedule, location, source, parentEvent, subtype,
				restrictions.toArray(new LtRestrictionsRestriction[0]));
	}

	/**
	 * Converts the feed to a TraFF message.
	 * 
	 * @param oldMessages Previously received messages
	 */
	public TraffMessage toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages) {
		TraffMessage.Builder builder = new TraffMessage.Builder();
		builder.setId(sourcePrefix + ":" + this.id);
		builder.setReceiveTime(creationtime);
		builder.setUpdateTime(updatetime);
		if (starttime.after(creationtime)) {
			/*
			 * it is unclear whether endtime refers to expected end time or expiration, therefore:
			 * when starttime is after creationtime (assumed to be the anticipated start), assume
			 * endtime to be expected end time, else assume it to be expiration time
			 */
			builder.setStartTime(starttime);
			builder.setEndTime(endtime);
		}
		else
			builder.setExpirationTime(endtime);

		/* start building the location */
		if (location != null) {
			TraffLocation.Builder locBuilder = new TraffLocation.Builder();
			if ("ONE_DIRECTION".equals(location.direction))
				locBuilder.setDirectionality(Directionality.ONE_DIRECTION);
			else
				locBuilder.setDirectionality(Directionality.BOTH_DIRECTIONS);
			locBuilder.setCountry("LT");
			String roadRef = null;
			String roadName = null;
			String roadDistance = null;
			if ((location.street != null) && location.street.contains(" ")) {
				roadRef = location.street.substring(0, location.street.indexOf(' '));
				roadName = location.street.substring(location.street.indexOf(' ') + 1,
						location.street.lastIndexOf(' '));
				if (roadRef.length() > 4)
					/* invalid ref, discard */
					roadRef = null;
				roadDistance = location.street.substring(location.street.lastIndexOf(' ') + 1,
						location.street.length());
			}
			locBuilder.setRoadRef(roadRef);
			String origin = null;
			String destination = null;
			if (roadName != null) {
				/*
				 * roadName is usually the route, separated by (real!!!) dashes.
				 * 
				 * Destinations abroad are given by their Lithuanian names and followed by an asterisk. We use
				 * them unchanged: for display, this results in a notation Lithuanian audiences would be familiar
				 * with, whereas for navigation, exonyms are useless anyway.
				 * 
				 * We exclude names which contain the word “kelias” in any inflected form, to avoid parsing names
				 * like “Privažiuojamasis kelias prie Patašinės nuo kelio Vilnius–Prienai–Marijampolė” into
				 * destinations “Privažiuojamasis kelias prie Patašinės nuo kelio Vilnius” and “Marijampolė”.
				 */
				if ((roadName != null) && roadName.contains("–") && !roadName.matches(".* kel(ias|io|iui|ią|iu|yje) .*")) {
					/*
					 * Coordinates, route description and kilometric points refer to the forward direction of
					 * the road, even for unidirectional locations referring to the opposite direction.
					 */
					origin = roadName.substring(0, roadName.indexOf('–'));
					locBuilder.setOrigin(origin);
					destination = roadName.substring(roadName.lastIndexOf('–') + 1, roadName.length());
					locBuilder.setDestination(destination);
				} else
					// TODO should we set roadName unconditionally?
					locBuilder.setRoadName(roadName);
			}
			int numPoints = -1;
			TraffLocation.Point from = null;
			TraffLocation.Point at = null;
			TraffLocation.Point to = null;
			if ((location.polyline != null) && (location.polyline.length > 0)) {
				if (location.polyline[0].distanceTo(location.polyline[location.polyline.length - 1]) < .001) {
					/* point location */
					TraffLocation.Point.Builder atBuilder = new TraffLocation.Point.Builder();
					atBuilder.setCoordinates(location.polyline[0]);
					if ((roadDistance != null) && (roadDistance.matches("(\\d+)(\\.(\\d+))?km(\\.?)"))) {
						Float distance = Float.valueOf(roadDistance.substring(0, roadDistance.indexOf("km")));
						atBuilder.setDistance(distance);
					}
					atBuilder.setJunctionName(getJunctionName(roadRef, location.polyline[0]));
					at = atBuilder.build();
					locBuilder.setAt(at);
					numPoints = 1;
				} else {
					TraffLocation.Point.Builder fromBuilder = new TraffLocation.Point.Builder();
					fromBuilder.setCoordinates(location.polyline[0]);
					TraffLocation.Point.Builder toBuilder = new TraffLocation.Point.Builder();
					toBuilder.setCoordinates(location.polyline[location.polyline.length - 1]);
					if ((location.polyline.length > 2)
							&& (location.polyline[0].distanceTo(location.polyline[location.polyline.length - 1]) > VIA_MIN_LENGTH)) {
						/* add via point (we might need if for locations affecting entire bypass roads) */
						double minDist = 0;
						LatLon via = null;
						for (int i = 1; i < location.polyline.length - 2; i++) {
							double newDist = Math.min(location.polyline[i].distanceTo(location.polyline[0]),
									(location.polyline[i].distanceTo(location.polyline[location.polyline.length - 1])));
							if (newDist > minDist) {
								via = location.polyline[i];
								minDist = newDist;
							}
						}
						if ((via != null) && (minDist > VIA_MIN_DIST)) {
							TraffLocation.Point.Builder viaBuilder = new TraffLocation.Point.Builder();
							viaBuilder.setCoordinates(via);
							locBuilder.setVia(viaBuilder.build());
						}
					}
					Float distanceFrom = null;
					if (roadDistance != null) {
						Pattern pattern = Pattern.compile("((\\d+)(\\.(\\d+))?)-((\\d+)(\\.(\\d+))?)km(\\.?)");
						Matcher matcher = pattern.matcher(roadDistance);
						if (matcher.matches()) {
							distanceFrom = Float.valueOf(matcher.group(1));
							Float distanceTo = Float.valueOf(matcher.group(5));
							fromBuilder.setDistance(distanceFrom);
							toBuilder.setDistance(distanceTo);
						}
					}
					/* add junction names */
					String fromName = getJunctionName(roadRef, location.polyline[0]);
					if ((fromName == null) && (distanceFrom != null) && (distanceFrom <= 0.5) && (origin != null))
						fromName = origin;
					String toName = getJunctionName(roadRef, location.polyline[location.polyline.length - 1]);
					if ((fromName == null) || (toName == null) || !fromName.equals(toName)) {
						/*
						 * Avoid pairings such as “between Lybiškės and Lybiškės” by not setting the name
						 * when the names for both points are non-null and equal.
						 * If both are null, setting them here is a no-op.
						 */
						fromBuilder.setJunctionName(fromName);
						toBuilder.setJunctionName(toName);
					}
					from = fromBuilder.build();
					locBuilder.setFrom(from);
					to = toBuilder.build();
					locBuilder.setTo(to);
					numPoints = 2;
				}
			}
			if (roadRef != null) {
				if (roadRef.startsWith("A")) {
					/* some A roads are motorways, find out if we are on a motorway section */
					TraffLocation.RoadClass roadClass = null;
					if (motorwayBboxes.containsKey(roadRef)) {
						int matched = 0;
						for (BoundingBox bbox : motorwayBboxes.get(roadRef)) {
							for (TraffLocation.Point point : new TraffLocation.Point[] {from, at, to})
								if ((point != null) && (bbox.contains(point.coordinates)))
									matched++;
							if (matched > 0)
								break;
						}
						if (matched == numPoints)
							/* the entire stretch is a motorway */
							roadClass = RoadClass.MOTORWAY;
						else if (matched > 0)
							/* part motorway, part trunk; use the predominant road class */
							roadClass = defaultRoadClassMappings.get(roadRef);
					}
					if (roadClass != null)
						/* set the road class we’ve determined */
						locBuilder.setRoadClass(roadClass);
					else
						/* or default to trunk */
						locBuilder.setRoadClass(RoadClass.TRUNK);
				} else
					if (roadRef.length() <= 3)
						locBuilder.setRoadClass(RoadClass.PRIMARY);
					else
						locBuilder.setRoadClass(RoadClass.OTHER);
			}
			/*
			 * For unidirectional locations, look for dešinė/kairė in the description and invert as needed.
			 * However, this information is often missing, and the directionality itself seems to be
			 * incorrect at times (bidirectional locations indicated as unidirectional).
			 * Coordinates are often precise enough to determine the carriageway on the map.
			 * For most of the problematic locations, they are on the forward carriageway, indicating that
			 * most of these locations are bidirectional (for unidirectional locations, one would expect to
			 * see either roughly half or all of them on the backward carriageway).
			 * For the moment, we assume the location to be bidirectional when the direction cannot be
			 * established.
			 */
			if ("ONE_DIRECTION".equals(location.direction)) {
				int direction = 0;
				if (description != null) {
				if (description.matches(".* [Kk]air(ė|ės|ei|ę|e|ėje) .*"))
					direction--;
				if (description.matches(".* [Dd]ešin(ė|ės|ei|ę|e|ėje) .*"))
					direction++;
				}
				if (direction < 0)
					locBuilder.invert();
				else if (direction == 0) {
					locBuilder.setDirectionality(Directionality.BOTH_DIRECTIONS);
					LOG.warn("{}: Cannot determine direction, assuming bidirectional location", id);
				}
			}
			builder.setLocation(locBuilder.build());
		}

		Map<TraffEvent.Type, TraffEvent> events = new HashMap<TraffEvent.Type, TraffEvent>();
		TraffEvent.Builder eventBuilder = new TraffEvent.Builder();

		if ("damagedBridge".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.HAZARD);
			eventBuilder.setType(TraffEvent.Type.HAZARD_BRIDGE_DAMAGE);
		} else if ("damagedViaduct".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.HAZARD);
			eventBuilder.setType(TraffEvent.Type.HAZARD_BRIDGE_DAMAGE);
		} else if ("damagedRoadSurface".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.HAZARD);
			eventBuilder.setType(TraffEvent.Type.HAZARD_BAD_ROAD);
		} else if ("fair".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.ACTIVITY);
			eventBuilder.setType(TraffEvent.Type.ACTIVITY_FAIR);
		} else if ("festival".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.ACTIVITY);
			eventBuilder.setType(TraffEvent.Type.ACTIVITY_FESTIVAL);
		} else if ("filmTVMaking".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.ACTIVITY);
			eventBuilder.setType(TraffEvent.Type.ACTIVITY_FILM_TV_PRODUCTION);
		} else if ("flooding".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.HAZARD);
			eventBuilder.setType(TraffEvent.Type.HAZARD_FLOODING);
		} else if ("motorSportRaceMeeting".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.ACTIVITY);
			eventBuilder.setType(TraffEvent.Type.ACTIVITY_AUTOMOBILE_RACE);
		} else if ("repairWork".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.CONSTRUCTION);
			eventBuilder.setType(TraffEvent.Type.CONSTRUCTION_MAINTENANCE);
		} else if ("sportsMeeting".equals(subtype)) {
			eventBuilder.setEventClass(TraffEvent.Class.ACTIVITY);
			eventBuilder.setType(TraffEvent.Type.ACTIVITY_SPORTS_EVENT);
		} else if ("CONSTRUCTION".equals(type)) {
			eventBuilder.setEventClass(TraffEvent.Class.CONSTRUCTION);
			eventBuilder.setType(TraffEvent.Type.CONSTRUCTION_ROADWORKS);
		} else if ("ACCIDENT".equals(type)) {
			eventBuilder.setEventClass(TraffEvent.Class.INCIDENT);
			eventBuilder.setType(TraffEvent.Type.INCIDENT_ACCIDENT);
		} else if ("HAZARD".equals(type)) {
			if (subtype.equals("damagedFlyover") || subtype.equals("weakBridge")) {
				eventBuilder.setEventClass(TraffEvent.Class.HAZARD);
				eventBuilder.setType(TraffEvent.Type.HAZARD_BRIDGE_DAMAGE);
			} else if (description.startsWith("Viešieji renginiai")) {
				eventBuilder.setEventClass(TraffEvent.Class.ACTIVITY);
				eventBuilder.setType(TraffEvent.Type.ACTIVITY_EVENT);
			}
			/* TODO
			else {
				eventBuilder.setEventClass(TraffEvent.Class.HAZARD);
				eventBuilder.setType(TraffEvent.Type.HAZARD_???);
			}
			*/
		}

		try {
			TraffEvent event = eventBuilder.build();
			events.put(event.type, event);
		} catch (IllegalStateException e) {
			LOG.warn("{}: Cannot create event for type {}, subtype {}; skipping", id, type, subtype);
		}

		for (LtRestrictionsRestriction restriction : restrictions) {
			eventBuilder = new TraffEvent.Builder();
			if ("forbidenTurn".equals(restriction.restrictionType)
					|| "forbiddenTurn".equals(restriction.restrictionType)) {
				/*
				 * the source sends a misspelled string, so accommodate both the incorrect spelling
				 * and the correct one
				 */
				// TODO
			} else if ("heightLimit".equals(restriction.restrictionType)) {
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_MAX_HEIGHT);
				try {
					eventBuilder.setQuantifier(new DimensionQuantifier(Float.valueOf(restriction.value)));
				} catch (NumberFormatException e) {
					LOG.warn("{}: \"{}\" is not a valid height limit", id, restriction.value);
				}
			} else if ("LD".equals(restriction.restrictionType)) {
				/* 
				 * Laikinis/ė d…?
				 * Most instances seem to be related to construction work, often indicating
				 * indicating restrictions while work is underway (though not all instances
				 * explicitly state that). It has also been seen for temporary checkpoints to
				 * enforce movement restrictions during the COVID-19 pandemic (which were in force
				 * only for part of the event duration).
				 */
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_RESTRICTIONS);
			} else if ("lengthLimit".equals(restriction.restrictionType)) {
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_MAX_LENGTH);
				try {
					eventBuilder.setQuantifier(new DimensionQuantifier(Float.valueOf(restriction.value)));
				} catch (NumberFormatException e) {
					LOG.warn("{}: \"{}\" is not a valid length limit", id, restriction.value);
				}
			} else if ("roadClosed".equals(restriction.restrictionType)) {
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_CLOSED);
			} else if ("traficJam".equals(restriction.restrictionType)) { // yes, the source has a typo in the string
				eventBuilder.setEventClass(TraffEvent.Class.CONGESTION);
				eventBuilder.setType(TraffEvent.Type.CONGESTION_TRAFFIC_CONGESTION);
			} else if ("speedLimit".equals(restriction.restrictionType)) {
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_SPEED_LIMIT);
				try {
					eventBuilder.setSpeed(Integer.valueOf(restriction.value));
				} catch (NumberFormatException e) {
					LOG.warn("{}: \"{}\" is not a valid speed limit", id, restriction.value);
				}
			} else if ("SVKD".equals(restriction.restrictionType)) {
				/* susiaurėjusi važiuojamoji kelio dalis */
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_NARROW_LANES);
			} else if ("weightLimit".equals(restriction.restrictionType)) {
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_MAX_WEIGHT);
				try {
					eventBuilder.setQuantifier(new WeightQuantifier(Float.valueOf(restriction.value)));
				} catch (NumberFormatException e) {
					LOG.warn("{}: \"{}\" is not a valid weight limit", id, restriction.value);
				}
			} else if ("widthLimit".equals(restriction.restrictionType)) {
				eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
				eventBuilder.setType(TraffEvent.Type.RESTRICTION_MAX_WIDTH);
				try {
					eventBuilder.setQuantifier(new DimensionQuantifier(Float.valueOf(restriction.value)));
				} catch (NumberFormatException e) {
					LOG.warn("{}: \"{}\" is not a valid width limit", id, restriction.value);
				}
			}

			TraffSupplementaryInfo si = null;
			if ("All".equals(restriction.transportType)) {
				// NOP
			} else if ("car".equals(restriction.transportType)) {
				si = new TraffSupplementaryInfo(TraffSupplementaryInfo.Class.VEHICLE,
						TraffSupplementaryInfo.Type.S_VEHICLE_CAR);
			} else if ("carWithTrailer".equals(restriction.transportType)) {
				// TODO should this be S_VEHICLE_WITH_TRAILER?
				si = new TraffSupplementaryInfo(TraffSupplementaryInfo.Class.VEHICLE,
						TraffSupplementaryInfo.Type.S_VEHICLE_CAR_WITH_TRAILER);
			} else if ("lorry".equals(restriction.transportType)) {
				si = new TraffSupplementaryInfo(TraffSupplementaryInfo.Class.VEHICLE,
						TraffSupplementaryInfo.Type.S_VEHICLE_HEAVY);
			} else {
				LOG.warn("{}: transport type \"{}\" is unknown", id, restriction.transportType);
			}
			if (si != null)
				eventBuilder.addSupplementaryInfo(si);

			try {
				TraffEvent event = eventBuilder.build();
				events.put(event.type, event);
			} catch (IllegalStateException e) {
				LOG.warn("{}: Cannot create event for restriction type {}; skipping",
						id, restriction.restrictionType);
				if ((si != null) && (restrictions.length == 1) && !events.isEmpty()) {
					// FIXME what if there are multiple unrecognized events, all with the same SI type?
					eventBuilder = new TraffEvent.Builder(events.values().iterator().next());
					eventBuilder.addSupplementaryInfo(si);
					TraffEvent event = eventBuilder.build();
					events.put(event.type, event);
					LOG.warn("{}: orphaned supplementary information {} was attached to main event",
							id, si.type);
				} else if (si != null)
					LOG.warn("{}: supplementary information {} could not be matched to an event; skipping",
							id, si.type);
			}
		}

		if (events.isEmpty())
			LOG.warn("{}: Could not determine any events; skipping message", id);
		builder.addEvents(events.values());

		try {
			return builder.build();
		} catch (IllegalStateException e) {
			return null;
		}
	}

	/**
	 * Determines if the point is near a junction, and if so, returns its name.
	 * 
	 * @param coords
	 * @return The junction name, or null
	 */
	private String getJunctionName(String roadRef, LatLon coords) {
		Map<String, Set<Junction>> roadJunctionsEismoinfo = junctionsEismoinfo.getByName(roadRef);
		Map<String, Set<Junction>> roadJunctionsOsm = junctionsOsm.getByName(roadRef);
		if ((roadJunctionsEismoinfo == null) && (roadJunctionsOsm == null))
			return null;
		Set<Junction> candidateJunctions = new HashSet<Junction>();
		/*
		 * Identify candidate junctions by their coordinates:
		 * If we have multiple junctions with the same name (usually a pair of junctions on opposite
		 * carriageways) and the endpoint is within their bounding box, each of these junction
		 * points is a candidate.
		 * Otherwise, every junction point within 0.5 km of the endpoint is a candidate.
		 */
		for (Map<String, Set<Junction>> roadJunctions : new Map[] {roadJunctionsEismoinfo, roadJunctionsOsm}) {
			if (roadJunctions == null)
				continue;
			for (Set<Junction> junctionSet : roadJunctions.values()) {
				BoundingBox bbox = null;
				for (Junction junction : junctionSet)
					if (bbox == null)
						bbox = new BoundingBox(junction.geo);
					else
						bbox = bbox.extend(junction.geo);
				if (bbox.contains(coords))
					candidateJunctions.addAll(junctionSet);
				else
					for (Junction junction : junctionSet)
						if (junction.geo.distanceTo(coords) < 0.5f)
							candidateJunctions.add(junction);
			}
			if (!candidateJunctions.isEmpty())
				break;
		}
		if (!candidateJunctions.isEmpty()) {
			Junction junction = JunctionUtils.mergeJunctions(candidateJunctions);
			return junction.name;
		}
		return null;
	}

	public LtRestrictionsMessage(String id, String type, Date creationtime, Date updatetime, Date starttime,
			Date endtime, String description, String shortDescription, Object schedule, LtRestrictionsLocation location,
			Object source, String parentEvent, String subtype, LtRestrictionsRestriction[] restrictions) {
		super();
		this.id = id;
		this.type = type;
		this.creationtime = creationtime;
		this.updatetime = updatetime;
		this.starttime = starttime;
		this.endtime = endtime;
		this.description = description;
		this.shortDescription = shortDescription;
		this.schedule = schedule;
		this.location = location;
		this.source = source;
		this.parentEvent = parentEvent;
		this.subtype = subtype;
		this.restrictions = restrictions;
	}
}
