#!/bin/bash
curl http://eismoinfo.lt/traffic-intensity-service | jq -r '["roadRef","km","name","lat","lon"],(.[]|[.roadNr, .km, .name, .x, .y]) | @csv' > `dirname $0`/../resources/org/traffxml/eismoinfo/restrictions/junctions-eismoinfo.csv
