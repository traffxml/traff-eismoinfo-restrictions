/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-restrictions library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.test.eismoinfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.traffxml.eismoinfo.restrictions.LtRestrictionsFeed;
import org.traffxml.eismoinfo.restrictions.LtRestrictionsMessage;
import org.traffxml.eismoinfo.restrictions.LtRestrictionsRestriction;
import org.traffxml.traff.TraffFeed;
import org.traffxml.viewer.TraffViewer;
import org.traffxml.viewer.input.ConverterSource;
import org.traffxml.viewer.input.StaticSource;

public class RestrictionsTest {
	private static final RestrictionsConverter CONVERTER = new RestrictionsConverter();
	private static final String DEFAULT_URL = "http://restrictions.eismoinfo.lt";
	private static final int FLAG_CONVERT = 0x1;
	private static final int FLAG_DISSECT = 0x2;
	private static final int FLAG_DUMP = 0x4;
	private static final int FLAG_GUI = 0x20000;
	private static final int MASK_OPERATIONS = 0xFFFF;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -convert <feed>  Convert <feed> to TraFF");
		System.out.println("  -dissect <feed>  Examine <feed> and print results");
		System.out.println("  -dump <feed>     Dump raw feed <feed>");
		System.out.println("  -gui             Launch TraFF Viewer UI (not with -dissect or -dump)");
		System.out.println();
		System.out.println("<feed> can refer to a local file or an http/https URL");
	}

	public static void main(String[] args) {
		LtRestrictionsFeed feed = null;
		String input = null;
		InputStream dataInputStream = null;
		ByteArrayOutputStream dataOutputStream = new ByteArrayOutputStream();
		int i;
		int flags = 0;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (i = 0; i < args.length; i++) {
			if ("-convert".equals(args[i])) {
				input = getParam("convert", args, ++i);
				flags |= FLAG_CONVERT;
			} else if ("-dissect".equals(args[i])) {
				input = getParam("dissect", args, ++i);
				flags |= FLAG_DISSECT;
			} else if ("-dump".equals(args[i])) {
				input = getParam("dump", args, ++i);
				flags |= FLAG_DUMP;
			} else if ("-gui".equals(args[i])) {
				flags |= FLAG_GUI;
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if (flags == 0) {
			System.err.println("A valid option must be specified.");
			System.exit(1);
		}
		if (flags == FLAG_GUI) {
			TraffViewer.launch(CONVERTER, DEFAULT_URL);
		} else {
			if (input.startsWith("http://") || input.startsWith("https://")) {
				URL url;
				try {
					url = new URL(input);
					if ((flags & FLAG_DUMP) == 0)
						feed = CONVERTER.parse(url);
					else
						dataInputStream = url.openStream();
				} catch (MalformedURLException e) {
					System.err.println("Input URL is invalid. Aborting.");
					System.exit(1);
				} catch (IOException e) {
					System.err.println("Cannot read from input URL. Aborting.");
					System.exit(1);
				}
			} else {
				File inputFile = new File(input);
				try {
					if ((flags & FLAG_DUMP) == 0)
						feed = CONVERTER.parse(inputFile);
					else
						dataInputStream = new FileInputStream(inputFile);
				} catch (FileNotFoundException e) {
					System.err.println("Input file does not exist. Aborting.");
					System.exit(1);
				} catch (IllegalArgumentException e) {
					System.err.println(String.format("%s. Aborting.", e.getMessage()));
					System.exit(1);
				}
			}
		}
		if ((feed != null) || (dataInputStream != null)) {
			switch(flags & MASK_OPERATIONS) {
			case FLAG_CONVERT:
				convertFeed(feed, (flags & FLAG_GUI) != 0);
				break;
			case FLAG_DISSECT:
				dissectFeed(feed);
				break;
			case FLAG_DUMP:
				try {
					i = dataInputStream.read();
					while (i != -1) {
						if (i >= 32) dataOutputStream.write(i);
						i = dataInputStream.read();
					}
					dataInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
				dump(dataOutputStream);
				break;
			default:
				System.err.println("Invalid combination of options. Aborting.");
				System.exit(1);
			}
		}
	}

	static void convertFeed(LtRestrictionsFeed feed, boolean gui) {
		try {
			TraffFeed tFeed = CONVERTER.convert(feed);
			if (gui) {
				StaticSource.readFeed(tFeed);
				TraffViewer.launch(CONVERTER, DEFAULT_URL);
			} else
				System.out.println(tFeed.toXml());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void dissectFeed(LtRestrictionsFeed feed) {
		Set<String> types = new HashSet<String>();
		Set<String> subtypes = new HashSet<String>();
		Set<String> restrictionTypes = new HashSet<String>();
		Set<String> transportTypes = new HashSet<String>();
		for (LtRestrictionsMessage message : feed.messages) {
			dissectMessage(message);
			types.add(message.type);
			subtypes.add(message.subtype);
			if (message.restrictions != null)
				for (LtRestrictionsRestriction restriction : message.restrictions) {
					restrictionTypes.add(restriction.restrictionType);
					transportTypes.add(restriction.transportType);
				}
		}
		System.out.println(String.format("Messages in feed:  %d", feed.messages.length));
		System.out.println(String.format("Types:             %d", types.size()));
		for (String type : types)
			System.out.println(String.format("  %s", type));
		System.out.println(String.format("Subtypes:          %d", subtypes.size()));
		for (String subtype : subtypes)
			System.out.println(String.format("  %s", subtype));
		System.out.println(String.format("Restriction types: %d", restrictionTypes.size()));
		for (String restrictionType : restrictionTypes)
			System.out.println(String.format("  %s", restrictionType));
		System.out.println(String.format("Transport types:   %d", transportTypes.size()));
		for (String transportType : transportTypes)
			System.out.println(String.format("  %s", transportType));
	}

	static void dissectMessage(LtRestrictionsMessage message) {
		System.out.println(String.format("Message: %s", message.id));
		System.out.println(String.format("  Updated: %s", (message.updatetime == null) ? "null" : message.updatetime.toString()));
		System.out.println(String.format("  Ends: %s", (message.endtime == null) ? "null" : message.endtime.toString()));
		if (message.location != null) {
			System.out.println("  Location:");
			System.out.println(String.format("    Direction: %s", message.location.direction));
			System.out.println(String.format("    Street: %s", message.location.street));
			System.out.println(String.format("    Description: %s", message.location.locationDescription));
			if (message.location.polyline != null) {
				if (message.location.polyline.length == 0)
					System.out.println("    Coords: empty");
				else if (message.location.polyline.length == 1) {
					/* never observed in practice, would also be caught by the next (first and last coord less than 1 m apart) */
					System.out.println("    Coords: single point");
					System.out.println(String.format("      %1$.6f, %2$.6f (https://www.openstreetmap.org/?mlat=%1$f&mlon=%2$f#map=13/%1$f/%2$f)",
							message.location.polyline[0].lat,message.location.polyline[0].lon));
				} else {
					if (message.location.polyline[0].distanceTo(message.location.polyline[message.location.polyline.length - 1]) < .001) {
						System.out.println(String.format("    Coords: coinciding points (%d)", message.location.polyline.length));
						System.out.println(String.format("      %1$.6f, %2$.6f (https://www.openstreetmap.org/?mlat=%1$f&mlon=%2$f#map=13/%1$f/%2$f)",
								message.location.polyline[0].lat, message.location.polyline[0].lon));
					} else {
						int last = message.location.polyline.length - 1;
						System.out.println(String.format("    Coords: true polyline (%d)", message.location.polyline.length));
						System.out.println(String.format("      (%1$.5f, %2$.5f)–(%3$.5f, %4$.5f) https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=%1$.6f%%2C%2$.6f%%3B%3$.6f%%2C%4$.6f#map=13/%5$.6f/%6$.6f",
								message.location.polyline[0].lat, message.location.polyline[0].lon,
								message.location.polyline[last].lat, message.location.polyline[last].lon,
								(message.location.polyline[0].lat + message.location.polyline[last].lat)/ 2.0f,
								(message.location.polyline[0].lon + message.location.polyline[last].lon)/ 2.0f));
					}
				}
			} else
				System.out.println("    Coords: null");
		} else
			System.out.println("  Location: null");
		System.out.println(String.format("  Type: %s, %s", message.type, message.subtype));
		System.out.println(String.format("  Restrictions: %d", message.restrictions.length));
		for (LtRestrictionsRestriction restriction : message.restrictions)
			System.out.println(String.format("    %s: %s %s", restriction.restrictionType,
					restriction.transportType, restriction.value));
		System.out.println(String.format("  Short Description: %s", message.shortDescription));
		System.out.println(String.format("  Description: %s", message.description));
		/*
		System.out.println(String.format("  Date: %s, interval %d", message.date, message.timeInterval));

		String route = "";
		if ((message.roadName != null) && message.roadName.contains("–")) {
			String origin = message.roadName.substring(0, message.roadName.indexOf('–'));
			String destination = message.roadName.substring(message.roadName.lastIndexOf('–') + 1, message.roadName.length());
			route = String.format(" (%s → %s)", origin, destination);
		}
		System.out.println(String.format("  Road: %s %s%s", message.roadNr, message.roadName, route));

		System.out.println(String.format("  At: km %.3f (%s)", message.km, message.name));
		System.out.println(String.format("  Coords: %1$.6f, %2$.6f (https://www.openstreetmap.org/?mlat=%1$f&mlon=%2$f#map=13/%1$f/%2$f)",
				message.x, message.y));
		if (message.roadSegments == null) {
			System.out.println("  Road segments: null");
		} else {
			System.out.println(String.format("  Road segments: %d", message.roadSegments.length));
			for (RoadSegment segment : message.roadSegments)
				dissectRoadSegment(segment);
		}
		*/
	}

	private static void dump(ByteArrayOutputStream stream) {
		JSONTokener tokener = new JSONTokener(stream.toString());
		while(true) {
			Object object = null;
			try {
				object = tokener.nextValue();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (object == null)
				break;
			parse(null, object, 0);
		}
		System.out.println("Done.");
	}

	private static String padding(int level) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < level; i++)
			builder.append(("  "));
		return builder.toString();
	}

	private static void parse(String key, Object object, int level) {
		if (object instanceof JSONObject) {
			parse(key, (JSONObject) object, level);
		} else if (object instanceof JSONArray) {
			parse(key, (JSONArray) object, level);
		} else if ((object instanceof String) || (object instanceof Boolean)
				|| (object instanceof Integer) || (object instanceof Long)
				|| (object instanceof Double)) {
			parse(key, object.getClass(), object.toString(), level);
		} else
			System.out.println(String.format("%s%s: (%s)", padding(level), key, object.getClass().getName()));
	}

	private static void parse(String key, JSONObject object, int level) {
		//System.out.println("JSONObject: " + ((JSONObject) object).toString());//
		System.out.println(String.format("%s%s: (JSONObject)", padding(level), key));
		Iterator<String> keys = object.keys();
		while (keys.hasNext()) {
			String chKey = keys.next();
			Object value = object.get(chKey);
			parse(chKey, value, level + 1);
		}
	};

	private static void parse(String key, JSONArray array, int level) {
		System.out.println(String.format("%s%s: (JSONArray)", padding(level), key));
		for (int j = 0; j < array.length(); j++) {
			Object object = array.get(j);
			parse(key, object, level + 1);
		}
	};

	private static void parse(String key, Class<? extends Object> clazz, String string, int level) {
		System.out.println(String.format("%s%s: (%s) %s", padding(level), key, clazz.getSimpleName(), string));
	}
	
	private static class RestrictionsConverter implements ConverterSource {

		@Override
		public TraffFeed convert(File file) throws Exception {
			LtRestrictionsFeed feed = parse(file);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(URL url) throws Exception {
			LtRestrictionsFeed feed = parse(url);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(Properties properties) throws Exception {
			throw new UnsupportedOperationException("This operation is not implemented yet");
		}

		private TraffFeed convert(LtRestrictionsFeed feed) {
			return new TraffFeed(feed.toTraff("test", null));
		}

		private LtRestrictionsFeed parse(File file) throws FileNotFoundException {
			if (!file.exists()) {
				throw new IllegalArgumentException("Input file does not exist");
			} else if (!file.isFile()) {
				throw new IllegalArgumentException("Input file is not a file");
			} else if (!file.canRead()) {
				throw new IllegalArgumentException("Input file is not readable");
			}
			return LtRestrictionsFeed.parseJson(new FileInputStream(file));
		}

		private LtRestrictionsFeed parse(URL url) throws IOException {
			return LtRestrictionsFeed.parseJson(url.openStream());
		}
	}
}
