[![Release](https://jitpack.io/v/org.traffxml/traff-eismoinfo-restrictions.svg)](https://jitpack.io/#org.traffxml/traff-eismoinfo-restrictions)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-eismoinfo-restrictions/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-eismoinfo-restrictions/javadoc/dev/.
