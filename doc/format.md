The format used by LAKD for traffic restrictions builds upon the [Waze CIFS 2.0 format](https://developers.google.com/waze/data-feed/feed-setup), with some customizations.

# Structure

Feeds are in JSON format; XML does not appear to be used.

The root element in CIFS 2.0 is a single JSON object, which contains a JSON array of incidents (messages in TraFF terminology) as its `incidents` element. LAKD omits this object and puts the array at the top.  

# Timestamps

All timestamps observed seem to specify their UTC offset.

# `location`

Can be null for some incidents.

# `polyline`

Occasionally includes pseudo-point locations consisting of two near-identical points (typically less than 1 m apart).

# `street`

LAKD seems to use a format of reference number, followed by road name or route description, followed by kilometric points, with spaces as separators for each.

# `type`

LAKD appears to use the standard CIFS types. The following have been observed:

| Type           | Remarks                     |
|----------------|-----------------------------|
| `CONSTRUCTION` |                             |
| `ACCIDENT`     | No subtypes observed yet    |
| `HAZARD`       | Also used for public events |

The `ROAD_CLOSED` type has not been observed and is possibly not used, as it is covered by the custom `restrictions` member (see further below).

# `location_description`

TBD

# `short_description`

LAKD seems to use a very systematic format of a generic and a more specific event description, two standardized strings separated by a space, hyphen and another space.

# `subtype`

LAKD does not appear to use any of the Waze subtypes. The following custom subtypes have been spotted:

| Subtype                 | Type           | Description                       |
|-------------------------|----------------|-----------------------------------|
| `damagedViaduct`        | `HAZARD`       | Damaged viaduct                   |
| `damagedRoadSurface`    | `HAZARD`       | Damaged or slippery road surface  |
| `fair`                  | `HAZARD`       | Fair                              |
| `festival`              | `HAZARD`       | Festival                          |
| `filmTVMaking`          | `HAZARD`       | Film or TV production             |
| `motorSportRaceMeeting` | `HAZARD`       | Motor sports competition          |
| `repairWork`            | `CONSTRUCTION` | Road repair work                  |
| `sportsMeeting`         | `HAZARD`       | Sports event                      |

# `restrictions`

LAKD introduces a non-standard element, `restrictions`, in the message element to provide details on traffic restrictions.

The `restrictions` element is an array of restrictions. It may be empty.

The child elements are detailed below,

## `restrictionType`

String, required. Specifies the type of restriction. The following have been observed:

| Type          | Description                                                       |
|---------------|-------------------------------------------------------------------|
| `LD`          | Temporary restrictions? Observed with `CONSTRUCTION`/`repairWork` |
| `roadClosed`  | Road is closed to the `transportType` specified                   |
| `speedLimit`  | A speed limit is in force                                         |
| `SVKD`        | Narrow carriageway (susiaurėjusi važiuojamoji kelio dalis)        |
| `weightLimit` | A weight limit is in force                                        |
| `widthLimit`  | A width limit is in force                                         |

## `value`

String, required where appropriate. Quantifies the restriction.

| `restrictionType` | Description                |
|-------------------|----------------------------|
| `LD`              | Not used                   |
| `roadClosed`      | Not used                   |
| `speedLimit`      | Speed limit in km/h        |
| `SVKD`            | Not used                   |
| `weightLimit`     | Weight limit in t (float?) |
| `widthLimit`      | Width limit in m (float)   |

## `transportType`

String, required. Specifies the vehicle types/modes of transportation to which the restriction applies. The following values have been observed:

| Type    | Description         |
|---------|---------------------|
| `All`   | All transport types |
| `lorry` | Trucks              |

# TraFF equivalents

## Subtypes

| Subtype                 | TraFF event                    |
|-------------------------|--------------------------------|
| `damagedViaduct`        | `HAZARD_BRIDGE_DAMAGE`*        |
| `damagedRoadSurface`    | `HAZARD_BAD_ROAD`*             |
| `fair`                  | `ACTIVITY_FAIR`*               |
| `festival`              | `ACTIVITY_FESTIVAL`*           |
| `filmTVMaking`          | `ACTIVITY_FILM_TV_PRODUCTION`* |
| `motorSportRaceMeeting` | `ACTIVITY_AUTOMOBILE_RACE`*    |
| `repairWork`            | `CONSTRUCTION_MAINTENANCE`*    |
| `sportsMeeting`         | `ACTIVITY_SPORTS_EVENT`*       |

*Proposed for TraFF v0.8

## Restrictions

| `restrictionType` | TraFF event                   | `value`               |
|-------------------|-------------------------------|-----------------------|
| `LD`              |                               |                       |
| `roadClosed`      | `RESTRICTION_CLOSED`          |                       |
| `speedLimit`      | `RESTRICTION_SPEED_LIMIT`     | `speed` attribute     |
| `SVKD`            | `RESTRICTION_NARROW_LANES`    |                       |
| `weightLimit`     | `RESTRICTION_TEMP_MAX_WEIGHT` | `q_weight` element    |
| `widthLimit`      | `RESTRICTION_TEMP_MAX_WIDTH`  | `q_dimension` element |

| `transportType` | TraFF Supplementary Information |
|-----------------|---------------------------------|
| `All`           | Optionally `S_VEHICLE_ALL`      |
| `lorry`         | `S_VEHICLE_HEAVY`               |

## Types

Only if subtype does not match.

| Type           | TraFF event                 |
|----------------|-----------------------------|
| `CONSTRUCTION` | `CONSTRUCTION_ROADWORKS`    |
| `ACCIDENT`     | `INCIDENT_ACCIDENT`         |
| `HAZARD`       | If `description` begins with "Viešieji renginiai": `ACTIVITY_EVENT`; else TODO |
